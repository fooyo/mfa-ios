# FooyoSDK

The following is one example of how Fooyo's SDK has been integrated to the Sentosa project.
The actual integration method may vary but the basic idea will be the same.

# Installation

## Carthage

[Carthage](https://github.com/Carthage/Carthage) is a decentralized dependency manager that builds your dependencies and provides you with binary frameworks.

You can install Carthage with [Homebrew](http://brew.sh/) using the following command:

```bash
$ brew update
$ brew install carthage
```

To integrate SnapKit into your Xcode project using Carthage, specify it in your `Cartfile`:

```ogdl
github "Pushian/FooyoTestSDK"
```

Run `carthage update` to build the framework and drag all the frameworks as well as the bundle file `FooyoSDK.bundle` inside `FooyoTestSDK.framework`
into your Xcode project.

# Usage

```swift
import FooyoTestSDK
```

## General SDK Functions

### Session Activation

```swift 
FooyoSDKOpenSession()
```

`GUIDE` Call this function whenever the app becomes active. All the data required by Fooyo SDK will be loaded at the background.

### User Login

```swift
FooyoSDKSignIn(userId: String)
```
`GUIDE` Call this function whenever the app achieves the user Id. All the user related data required by Fooyo SDK will be loaded at the background.

### User Logout

```swift
FooyoSDKSignOut()
```
`GUIDE` Call this function whenever the app achieves the user Id. All the user related data locally saved by Fooyo SDK will be removed.


## BaseMap SDK Function

### Initialization

```swift
let vc = FooyoBaseMapViewController()
vc.delegate = self
```


### Delegate Function

Delegate Prototal: `FooyoBaseMapViewControllerDelegate`.

Delegate Function: `To Be Determined`
